# To Do List: #

## TEMPORARILY FIXED
* FIXED ladder-platform interaction (NEED BETTER FIX)

## HIGH PRIORITY: ##
* Boss AI / Non-stupid grunt AI
* Game mode implementation

## MEDIUM PRIORITY: ##
* Buffs/Skills
* Proper HUD implementation

## LOW PRIORITY: ##
* Fix ladders and platform <- See Temp fixed

## IF HAVE TIME: ##
* Interactable entities
* Dialogue boxes

## IF WE REALLY HAVE TIME: ##
* Qayum coins
* Drag and drop skill sets